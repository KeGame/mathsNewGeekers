package com.gy.sams.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;

import com.gy.sams.dao.IUserDao;
import com.gy.sams.dao.baseDao;
import com.gy.sams.entity.User;

public class UserDaoImpl extends baseDao implements IUserDao {
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	/*
	 * 返回用户
	 */
	@Override
	public User findUser(User user) {
		// TODO Auto-generated method stub\
		/*
		 1.定义sql
		 2.连接数据库，执行sql,关闭
		 3.返回查询结果
		 */
		//1.定义sql
		String sql="select * from _user where _user.uname=? and _user.upass=? and _user.role=?";
		//2.连接数据库，执行sql,关闭
		User user1=null;
		try {
			conn=this.getConn();
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, user.getUname());
			pstmt.setString(2, user.getUpass());
			pstmt.setString(3, user.getRole());
			rs=pstmt.executeQuery();
			while(rs.next()) {
				user1=new User();
				user1.setId(rs.getString("id"));
				user1.setUname(rs.getString("uname"));
				user1.setUpass(rs.getString("upass"));
				user1.setRole(rs.getString("role"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("查找用户异常！");
			e.printStackTrace();
		}
		//3.返回查询结果
		return user1;
	}
	/*
	 * 添加用户
	 */
	@Override
	public User addUser(String name, String pass,String role) {
		// TODO Auto-generated method stub
		/*
		 1.定义sql
		 2.连接数据库，执行sql,关闭
		 3.返回查询结果
		 */
		//1.定义sql
		String sql="insert into _user values(?,?,?,?)";
		User user=new User();
		//2.连接数据库，执行sql,关闭
		try {
			conn=this.getConn();
			pstmt=conn.prepareStatement(sql);
			String id=UUID.randomUUID().toString();
			pstmt.setString(1, id);
			pstmt.setString(2, name);
			pstmt.setString(3, pass);
			pstmt.setString(4, role);
			pstmt.executeUpdate();
			user.setId(id);
			user.setUname(name);
			user.setUpass(pass);
			user.setRole(role);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("添加用户异常");
			e.printStackTrace();
		}finally {
			this.closeAll(conn, pstmt, rs);
		}
		//3.返回查询结果
		return user;
		
	}
	/*
	 * 删除用户
	 */
	@Override
	public int deleteUser(String id) {
		// TODO Auto-generated method stub
		/*
		 * 1.定义sql
			2.连接数据库，执行sql,关闭
			 3.返回更新执行次数
		 */
		//1.定义sql
		String sql="delete from _user where id=?";
		//2.连接数据库，执行sql,关闭
		int num=0;
		try {
			conn=this.getConn();
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, id);
			num=pstmt.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("删除用户异常");
			e.printStackTrace();
		}finally {
			this.closeAll(conn, pstmt, null);
		}
		return num;		
	}
	
}
