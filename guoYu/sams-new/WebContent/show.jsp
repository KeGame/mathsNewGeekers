<%@ page language="java"
	import="com.gy.sams.entity.*,com.gy.sams.dao.*,com.gy.sams.dao.impl.*"  
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	User user=null;
	if(session.getAttribute("user")!=null){
		user=(User)session.getAttribute("user");
	}
%>
			<%
				String id=request.getParameter("id");
				IScoreDao scoreDao=new ScoreDaoImpl();
				
				Score score=scoreDao.getScore(id);
						
			%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>修改</title>
<style type="text/css">
.info a {
	text-decoration: none;
	color: #F93
}

.info a:hover {
	color: #F00;
	font-weight: bold
}

.info {
	float: right;
	line-height: 30px;
}

.main {
	clear: both;
	background: #E8FAD6;
	height: 350px;
}

.content {
	margin-left: 30px;
}
</style>
</head>

<body>
	<div class="info">当前用户：<%if(user!=null){%><%=user.getUname()%><%}%>[<a href="Do/doOut.jsp">登出</a>]</div>
	<div style="margin: 0 auto; width: 700px;">
		
		<div class="main">
			<h2
				style="text-align: center; margin: 0; padding: 0px; line-height: 80px;">修改学生成绩</h2>
			<div class="content">
				<form name="form" method="post" action="Do/doUpdate.jsp"
					onsubmit="return postForm()">
					<table border="0">
						
						<tr>		
											
							<td><label><input type="hidden" name="gid" value="<%=score.getUser_id()  %>" /></label></td>
							
							<td><label> 姓名：<input type="text" name="gname" value="<%=score.getSname() %>" /></label></td>
							
							<td><label> 分数：<input type="text" name="gscore" value="<%=score.getSscore() %>" /></label></td>
						</tr>
						
						<tr>
							<td ><label> <input
									type="submit" name="button" id="button" value="修改" />
							</label></td>
						</tr>
					</table>
				</form>
			</div>
			<!--content-->
		</div>
	</div>
</body>
</html>