<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>登陆</title>
<script type="text/javascript">
function postForm()
{
	var name=document.form.uName.value;
	var pass=document.form.uPass.value;
	var id=document.form.identity.value;
	if(name==""||name==" "){
		alert("请填写学号！");
		return false;
	}else if(pass==""||pass==" "){
		alert("请输入密码！");
		return false;
	}/* else if(){
		
	} */
	return true;
}
</script>
</head>
<body>
	<div style="margin: 0 auto ;width: 500px;">
		<div style="border: 2px #0C9 solid; width:500px; height:255px; margin-top:150px; background:#DFEDB4">
			<h2 style="width:250px;margin:0 auto">学生成绩管理系统</h2>
			<div style="width:170px;margin:0 auto">
				<form name="form" method="post" action="Do/doLogin.jsp"
					onsubmit="return postForm()" style="margin:0;padding:0">
					<table width="170px" border="0">
						<tr>
							<td>学号：<label><input type="text" name="uId"/></label></td>
						</tr>						
						<tr>
							<td>姓名：<label><input type="text" name="uName"/></label></td>
						</tr>						
						<tr>
							<td>密码：<label><input type="password" name="uPass"/></label></td>
						</tr>
						<tr>
							<td>身份：<select name="uRole">
							<option value="0">学生</option>
							<option value="1">老师</option>
							</select></td>
						</tr>
						<tr>
							<td style="text-align:center"><label><input type="submit" name="button" id="button" value="登陆"/></label></td>
						</tr>
					</table>
					<p>
						<br/>
					</p>
				</form>			
			</div>
		</div>
	</div>
</body>
</html>