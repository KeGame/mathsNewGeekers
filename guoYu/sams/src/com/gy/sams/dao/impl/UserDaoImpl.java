package com.gy.sams.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.gy.sams.dao.IUserDao;
import com.gy.sams.dao.baseDao;
import com.gy.sams.entity.User;

public class UserDaoImpl extends baseDao implements IUserDao {
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	/*
	 * 返回用户
	 */
	@Override
	public User findUser(User user) {
		// TODO Auto-generated method stub\
		/*
		 1.定义sql
		 2.连接数据库，执行sql,关闭
		 3.返回查询结果
		 */
		//1.定义sql
		String sql="select * from _user where _user.id=?";
		//2.连接数据库，执行sql,关闭
		User user1=null;
		try {
			conn=this.getConn();
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, user.getId());
			rs=pstmt.executeQuery();
			while(rs.next()) {
				user1=new User();
				user1.setId(rs.getString("id"));
				user1.setUname(rs.getString("uname"));
				user1.setUpass(rs.getString("upass"));
				user1.setRole(rs.getString("role"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("查找用户异常！");
			e.printStackTrace();
		}
		//3.返回查询结果
		return user1;
	}
	
}
