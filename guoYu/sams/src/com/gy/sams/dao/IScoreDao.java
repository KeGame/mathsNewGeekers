package com.gy.sams.dao;

import java.util.List;

import com.gy.sams.entity.Score;

public interface IScoreDao {
	/*
	 * 返回单个学生成绩
	 */
	public Score getScore(String id) throws Exception;
	/*
	    返回所有学生成绩。类型是List<Score>
	 */
	public List<Score> getAllScores() throws Exception;
	/*
	 删除单个学生成绩，类型是int
	 */
	public int deleteScore(String id) throws Exception;
	/*
	 * 增加学生成绩，类型是int
	 */
	public int addScore(String id,String sname,String sscore) throws Exception;
	/*
	 * 修改单个学生成绩，类型是int
	 */
	public int updateScore(String id) throws Exception;
	
}
